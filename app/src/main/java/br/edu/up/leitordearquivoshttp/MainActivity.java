package br.edu.up.leitordearquivoshttp;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.VideoView;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

public class MainActivity extends AppCompatActivity {

  EditText txtDados;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    txtDados = (EditText) findViewById(R.id.txtDados);
  }

  public void onClickBaixarTexto(View v){

    BaixarTextoTask task = new BaixarTextoTask();
    task.execute();

  }

  public void onClickTocarVideo(View v){

    VideoView vv = (VideoView) findViewById(R.id.videoView);
    vv.setVideoPath("https://drive.google.com/uc?export=view&id=0BwO0-hA9fDiUZ2t5OTdUY3Z4ZEk");
    vv.setMediaController(new MediaController(this));
    vv.start();
  }

  public void onClickTocarMusica(View v){

    MediaPlayer mediaPlayer = new MediaPlayer();
    try {
      mediaPlayer.setDataSource("https://drive.google.com/uc?export=view&id=0BwO0-hA9fDiUV0FDVk9BWXAwOVU");
      mediaPlayer.prepare();
      mediaPlayer.start();

    } catch (IOException e) {
      e.printStackTrace();
    }

  }

  class BaixarTextoTask extends AsyncTask<String, String, String> {

    @Override
    protected String doInBackground(String... strings) {

      String texto = new String();
      String txt = "https://drive.google.com/uc?export=view&id=0BwO0-hA9fDiUMTFVS1NkVVhjaFk";

      try {
        URL url = new URL(txt);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        InputStream is = con.getInputStream();
        Scanner leitor = new Scanner(is);

        while(leitor.hasNext()){
          String linha = leitor.nextLine();
          texto += linha + "\n";
        }
        leitor.close();
        is.close();

      } catch (Exception e) {
        e.printStackTrace();
      }
      return texto;
    }

    @Override
    protected void onPostExecute(String resultado) {
      txtDados.setText(resultado);
    }
  }

  ImageView imageView;

  public void onClickBaixarPokemon(View v){

    imageView = (ImageView) findViewById(R.id.imageView);

    String str = "https://drive.google.com/uc?export=view&id=0BwO0-hA9fDiUQ1FOMUJhNUpIY1U";
    try {
      URL url = new URL(str);

      BaixarImagemTask task = new BaixarImagemTask();
      task.execute(url);

    } catch (MalformedURLException e) {
      e.printStackTrace();
    }
  }

  class BaixarImagemTask extends AsyncTask<URL, String, Bitmap> {

    @Override
    protected Bitmap doInBackground(URL... urls) {

      Bitmap bitmap = null;
      URL url = urls[0];

      try {
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        InputStream is = con.getInputStream();

        bitmap = BitmapFactory.decodeStream(is);
        is.close();

      } catch (IOException e) {
        e.printStackTrace();
      }
      return bitmap;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap){
      imageView.setImageBitmap(bitmap);
    }
  }


}